import React, { useState } from 'react';
import AddPage from '../../componets/party/add';
import { Link } from "react-router-dom";
import './sidebar.css';

const SideBar = () => {

    return (
        <div className="navbar-default sidebar" role="navigation">
            <div className="sidebar-nav navbar-collapse">
                <ul className="nav" id="side-menu">
                    <li>
                        <a href=""><i className="fa fa-user nav_icon"></i>Party<span className="fa arrow"></span></a>
                        <ul className="nav nav-second-level">
                            <li>
                                <Link to="/party/new">New Party</Link>
                            </li>
                            <li>
                                <Link to="/party/edit">View/Edit Employee</Link>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href=""><i className="fa fa-user nav_icon"></i>Stock<span
                            className="fa arrow"></span></a>
                        <ul className="nav nav-second-level">
                            <li>
                                <Link to="/party/new">New Stock</Link>
                            </li>
                            <li>
                                <Link to="/party/edit">View/Edit Stock</Link>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href=""><i className="fa fa-user nav_icon"></i>Stock Management<span className="fa arrow"></span></a>
                        <ul className="nav nav-second-level">
                            <li>
                                <Link to="/">New Item</Link>
                            </li>
                            <li>
                                <Link to="/">View/ Edit Item</Link>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    );
}
export default SideBar;