/*import logo from './logo.svg';*/
import './App.css';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import EditPage from './componets/party/edit';
import HomePage from './componets/home/home';
import Test1 from './componets/test/test1';
import PartyNew from './componets/party/party_new';
import PartyView from './componets/party/party_view';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" ><HomePage /></Route>

        <Route exact path="/party/new" ><PartyNew /></Route>
        <Route exact path="/party/edit"><EditPage /></Route>
        <Route exact path="/party/view"><PartyView /></Route>

        <Router exact path="/test1"><Test1 /></Router>

      </Switch>
    </Router>
  );
}
export default App;