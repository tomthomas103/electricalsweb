import Header2 from "../../common/header/header2";
import Sidebar1 from "../../common/sidebar/sidebar1";

const Test1 = () => {
    return (
        <div>
            <Header2 />
            <Sidebar1 />

            <li><a>hide tabs</a></li>
            <li><a>hide menu</a></li>
            <li className="divider"></li>
            <li><a>wordwrap</a></li>
            <li><a>line numbers</a></li>
            <li><a>fullscreen</a></li>
            <li className="divider"></li>
            <li><a>highlight active line</a></li>
            <li>
                <a>sidebar</a>
                <ul className="submenu">
                    <li><a>hide sidebar</a></li>
                </ul>
            </li>
        </div>

    );
}
export default Test1;